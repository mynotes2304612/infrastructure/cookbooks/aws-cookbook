#!/bin/bash
#
# Student: Lozano Gustavo - 20161317K
#
# This script automate the creation of 
# resources in "Task 5: Launch a Bastion Server in the Public Subnet" of 
# Module7: Lab 4 - Configure VPC (AWS Cloud Operations course)


source vars.sh # load needed variables of other tasks

INSTANCE_NAME='Bastion Server'
INSTANCE_TYPE='t2.micro'
KEY_PAIR='vockey'
SECURITY_GROUP_NAME='BastionSG'
SUBNET_NAME=$PUBLIC_SUBNET_NAME


# Getting VPC id
vpcId=$(aws ec2 describe-vpcs \
          --filters "Name=tag:Name,Values=$VPC_NAME" \
                    "Name=state,Values=available" \
          --query 'Vpcs[*].VpcId' \
          --output text)

if [[ -z $vpcId ]]; then
  echo "[+] No $VPC_NAME VPC was created!"
  exit 1
fi

echo "[*] $VPC_NAME VPC: $vpcId"

# SSH security group - PROBLEM: AWS is searching in default VPC, Despite we are passing vpcId as filter
sg=$(aws ec2 describe-security-groups \
        --filter "Name=vpc-id,Values=$vpcId" \
        --group-names $SECURITY_GROUP_NAME \
        --query "SecurityGroups[*].GroupId" \
        --output text 2> /dev/null)

if [[ -z $sg ]]; then
    echo "[*] Creating $SECURITY_GROUP_NAME security group"
    sg=$(aws ec2 create-security-group \
          --group-name $SECURITY_GROUP_NAME \
          --description $SECURITY_GROUP_NAME \
          --vpc-id $vpcId \
          --query GroupId \
          --output text)
fi

echo "$SECURITY_GROUP_NAME security group: $sg"

# Adding ingress to SSH security group
aws ec2 authorize-security-group-ingress \
  --group-id $sg \
  --protocol tcp \
  --port 22 \
  --cidr 0.0.0.0/0 2> /dev/null


# Launching instance
ami=$(aws ssm get-parameter \
        --name /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2 \
        --query "Parameter.Value" \
        --output text)

echo "AMI: $ami"

instanceId=$(aws ec2 describe-instances \
              --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
                        "Name=instance-state-name,Values=running" \
              --query 'Reservations[*].Instances[*].InstanceId' \
              --output text)

if [[ -z $instanceId ]]; then
  echo "Creating instance $INSTANCE_NAME"

  subnetId=$(aws ec2 describe-subnets \
                    --filters "Name=tag:Name,Values=$SUBNET_NAME" \
                    --query 'Subnets[*].SubnetId' \
                    --output text)

  if [[ -z $subnetId ]];then
    echo "[-] No $SUBNET_NAME subnet was created!!"
    exit 1
  fi

  instanceId=$(aws ec2 run-instances \
                --image-id $ami \
                --instance-type $INSTANCE_TYPE \
                --key-name $KEY_PAIR \
                --security-group-ids $sg \
                --subnet-id $subnetId \
                --associate-public-ip-address \
                --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=$INSTANCE_NAME}]" \
                --query 'Instances[*].InstanceId' \
                --output text)
fi

echo "Waiting for instance $instanceId ..."
aws ec2 wait instance-running --instance-ids $instanceId

aws ec2 describe-instances --instance-ids $instanceId \
    --filters 'Name=instance-state-name,Values=running' \
    --query 'Reservations[*].Instances[*]' \
    | grep -E "KeyName|PublicIpAddress|Platform|PublicDnsName" | uniq