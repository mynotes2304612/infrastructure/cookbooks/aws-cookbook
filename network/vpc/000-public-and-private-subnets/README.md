## Public and Private Subnets

Module: Module 7: Lab 4 - Configure VPC
Course: AWS Cloud Operations

This recipe deploys the following infrastructure:

![Public-and-Private-subnets](./images/network-infrastructure.png)
