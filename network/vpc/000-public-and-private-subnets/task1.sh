#!/bin/bash
#
# Student: Lozano Gustavo - 20161317K
#
# This script automate the creation of 
# resources in task1 of Module7: Lab 4 - Configure VPC (AWS Cloud Operations course)

VPC_NAME='Lab VPC'
VPC_CIDR='10.0.0.0/16'


vpcId=$(aws ec2 describe-vpcs \
          --filters "Name=tag:Name,Values=$VPC_NAME" \
                    "Name=cidr,Values=$VPC_CIDR" \
                    "Name=state,Values=available" \
          --query 'Vpcs[*].VpcId' \
          --output text)

if [[ -z $vpcId ]];then
  echo "[+] Creating $VPC_NAME VPC"
  vpcId=$(aws ec2 create-vpc \
            --cidr-block $VPC_CIDR \
            --tag-specifications "ResourceType=vpc,Tags=[{Key=Name,Value=$VPC_NAME}]" \
            --query 'Vpc.VpcId' \
            --output text)
fi

echo "$VPC_NAME: $vpcId"