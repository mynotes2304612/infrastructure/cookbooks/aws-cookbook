#!/bin/bash
#
# This script check the status of a target group and its load balancer  attached

################################ SCRIPT VARIBLES ################################

LOAD_BALANCER_NAME=LabELB
TARGET_GROUP_NAME=LabGroup

################################################################################################

targetGroupArn=$(aws elbv2 describe-target-groups \
                  --names $TARGET_GROUP_NAME \
                  --query 'TargetGroups[].TargetGroupArn' \
                  --output text)

if [[ -z $targetGroupArn ]];then
  echo "[*] Target Group $TARGET_GROUP_NAME does not exist"
  exit 1
fi

echo "[*] Heath of instances registered to $TARGET_GROUP_NAME target group"
aws elbv2 describe-target-health \
  --target-group-arn $targetGroupArn \
  --query 'TargetHealthDescriptions[]'


# testing http load balancer (workload destribution)
lbDNSName=$(aws elbv2 describe-load-balancers \
              --query "LoadBalancers[?LoadBalancerName==\`$LOAD_BALANCER_NAME\`].DNSName" \
              --output text)
N=10
url="http://$lbDNSName"
echo "[*] Making $N requests to $url"
for i in $(seq $N);do
  instanceId=$(curl -s $url | grep -E "i-[a-z0-9]*" -o)
  echo "response from instance: $instanceId"
done
