resource "aws_db_subnet_group" "db_subnet_group" {
  description = "DB Subnet Group"
  subnet_ids  = [aws_subnet.subnet_private1.id, 
                  aws_subnet.subnet_private2.id]

  tags = {
    Name = "${var.db_engine}-DB-Subnet-Group"
  }
}


resource "aws_db_instance" "primary_db" {
  allocated_storage       = 20
  db_name                 = var.db_name
  engine                  = var.db_engine
  engine_version          = var.db_engine_version
  instance_class          = var.db_instance_type
  username                = var.db_user
  password                = var.db_password
  parameter_group_name    = "default.${var.db_engine}${var.db_engine_version}"

  multi_az                = true
  vpc_security_group_ids  = [aws_security_group.allow_db_access.id]
  db_subnet_group_name    = aws_db_subnet_group.db_subnet_group.name
  skip_final_snapshot     = true
  backup_retention_period = 7

  tags = {
    Name = "primary-db-instance"
  }
}

resource "aws_db_instance" "replica_db" {
  count                   = var.number_db_replicas
  replicate_source_db     = aws_db_instance.primary_db.identifier
  identifier              = "db-replica-${count.index}"
  skip_final_snapshot     = true
  instance_class          = var.db_instance_type

  tags = {
    Name = "replica-db-instance${count.index}"
  }
}