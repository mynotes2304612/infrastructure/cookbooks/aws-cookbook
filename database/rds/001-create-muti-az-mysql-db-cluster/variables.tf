variable "db_name" {
  description = "Database name"
  type        = string
  default     = "db"
}

variable "db_user" {
  description = "RDS user"
  type        = string
  sensitive   = true
}

variable "db_password" {
  description = "RDS user password"
  type        = string
  sensitive   = true
}

# To select a suitable RDS engine version, check the following document:
# -
variable "db_engine_version" {
  description = "RDS Engine version"
  type        = string
  default     = "5.7"
}

variable "db_engine" {
  description = "RDS Engine"
  type        = string
  default     = "mysql"
}


# To select a suitable RDS instance type, check the following document:
# - https://aws.amazon.com/rds/instance-types/
variable "db_instance_type" {
  description = "RDS instance type"
  type        = string
  default     = "db.t3.micro"
}

variable "number_db_replicas" {
  description = "Number of DB replicas"
  type        = number
  default     = 2
}