## VPC
resource "aws_vpc" "main" {
  cidr_block            = "10.2.0.0/16"
  enable_dns_support    = true
  enable_dns_hostnames  = true
  
  tags = {
    Name = "rds-${var.db_engine}-vpc"
  }
}


## Subnets
resource "aws_subnet" "subnet_public1" {
  vpc_id      = aws_vpc.main.id
  cidr_block  = cidrsubnet(aws_vpc.main.cidr_block, 8, 0)
  availability_zone = "us-east-1a"

  tags = {
    "Name" = "subnet-public1"
  }
}

resource "aws_subnet" "subnet_private1" {
  vpc_id      = aws_vpc.main.id
  cidr_block  = cidrsubnet(aws_vpc.main.cidr_block, 8, 1)
  availability_zone = "us-east-1a"
  map_public_ip_on_launch = false

  tags = {
    "Name" = "subnet-private1"
  }
}

resource "aws_subnet" "subnet_private2" {
  vpc_id      = aws_vpc.main.id
  cidr_block  = cidrsubnet(aws_vpc.main.cidr_block, 8, 3)
  availability_zone = "us-east-1b"

  tags = {
    "Name" = "subnet-private2"
  }
}

# Network connection resources

## Route tables

resource "aws_route_table" "rtb_public" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name        = "rtb-public"
  }
}

# resource "aws_route_table" "rtb_private" {
#   vpc_id = aws_vpc.main.id

#   tags = {
#     Name        = "rtb-private"
#   }
# }

## Routes

resource "aws_route" "public_internet_gateway" {
  route_table_id         = aws_route_table.rtb_public.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}

# resource "aws_route" "private_nat_gateway" {
#   route_table_id         = aws_route_table.rtb_private.id
#   destination_cidr_block = "0.0.0.0/0"
#   nat_gateway_id         = aws_nat_gateway.nat_public1.id
# }

## Route table association - rta

resource "aws_route_table_association" "rta_public1" {
  subnet_id      = aws_subnet.subnet_public1.id
  route_table_id = aws_route_table.rtb_public.id
}

# resource "aws_route_table_association" "rta_public2" {
#   subnet_id      = aws_subnet.subnet_public2.id
#   route_table_id = aws_route_table.rtb_public.id
# }

# resource "aws_route_table_association" "rta_private1" { 
#   subnet_id      = aws_subnet.subnet_private1.id
#   route_table_id = aws_route_table.rtb_private.id
# }

# resource "aws_route_table_association" "rta_private2" { 
#   subnet_id      = aws_subnet.subnet_private2.id
#   route_table_id = aws_route_table.rtb_private.id
# }

## Internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = "igw"
  }
}

## NAT gateway
# resource "aws_eip" "nat_gw_eip" {
#   vpc = true
# }

# resource "aws_nat_gateway" "nat_public1" {
#   allocation_id = aws_eip.nat_gw_eip.id
#   subnet_id     = aws_subnet.subnet_public1.id
#   depends_on    = [aws_internet_gateway.igw]

#   tags = {
#     "Name" = "nat-public1"
#   }
# }