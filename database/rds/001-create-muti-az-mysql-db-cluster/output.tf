output "primary_db_endpoint" {
  value = aws_db_instance.primary_db.endpoint
}