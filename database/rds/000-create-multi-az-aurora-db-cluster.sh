#!/bin/bash
#
# This script deploy a multi-az db cluster on a selected VPC
#
# IMPORTANT:
#
# - When you create a DB Cluster using Amazon Console (WEB), it automaticaly create primary instance
#   for your cluster but with AWS CLI, it doesn't, 
#   so we need to create it manually (best for us - more to learn!!!)


################################ SCRIPT VARIBLES ################################

# NOTE: 
# This script doesn't create any VPC or any subnet (this infrastructure must already exists)
# - Your can deploy a VPC with multi-az subnets with recipe network/vpc/001-public-and-private-subnets-multi-az
VPC_ID="vpc-03390debb3f3a1e34" # VPC where the db cluster will be deployed

# VPC_SUBNETS possible values:
# -1                  - mean all subnets in VPC
# (Subnet Name ....)  - list of all subnet names on which the db cluster will be deployed
#
# NOTE: 
# - When supply subnet names, ensure that at least the subnets belong to 2 different availability zones.
VPC_SUBNETS=('subnet-private1' 'subnet-private2' 'subnet-private3')

# Security Group
DB_SG_NAME='dbSG' # security group to allow traffic to db cluster
DB_SG_CIDR_INGRESS='0.0.0.0/0'
DB_SG_DESCRIPTION='Security group to allow traffic to aurora db cluster'

# Subnet Group
DB_SUBNET_GROUP_NAME='mydbsubnet'
DB_SUBNET_GROUP_DESCRIPTION='Default-DB-SubnetGroup'

# DB Cluster Specifications
DB_CLUSTER_NAME="example"
DB_ENGINE='aurora-mysql'

# NOTE: To select a suitable engine version, read the following guide:
# - https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/Aurora.VersionPolicy.html
DB_ENGINE_VERSION=5.7

# NOTE: To select a suitable instance type, read the following guides:
# - https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/Concepts.DBInstanceClass.html
# - https://aws.amazon.com/rds/instance-types/
DB_CLUSTER_INSTANCES_TYPE=db.r5.large # type of instances in DB cluster 

# number of db instances on db cluster (read instances)
DB_INSTANCES=2 

################################################################################################


echo "[+] DB cluster name: $DB_CLUSTER_NAME"

vpcId=$VPC_ID
# networks
# vpcId=$(aws ec2 describe-vpcs \
#         --filters "Name=tag:Name,Values=$VPC_NAME" \
#         --query 'Vpcs[*].VpcId' \
#         --output text)

echo "VPC: $vpcId"

if [[ $VPC_SUBNETS == -1 ]]; then # use all subnets in selected VPC
  subnetsId=$(aws ec2 describe-subnets \
                --filter "Name=vpc-id,Values=$vpcId" \
                --query "Subnets[].SubnetId" \
                --output text)
else # getting IDs of selected subnets
  subnetsId=$(aws ec2 describe-subnets \
                    --filters "Name=vpc-id,Values=$vpcId" \
                              "Name=tag:Name,Values=$(echo ${VPC_SUBNETS[@]} | sed 's/ /,/g')" \
                    --query 'Subnets[].SubnetId' \
                    --output text)
fi


echo "[*] Subnets: $subnetsId"


# DB security group (port: 3306, engine: aurora-mysql)
sgId=$(aws ec2 describe-security-groups \
          --filters "Name=group-name,Values=$DB_SG_NAME" \
                    "Name=vpc-id,Values=$vpcId" \
          --query "SecurityGroups[].GroupId" \
          --output text)

if [[ -z $sgId ]]; then
  echo "Creating DB security group"
  sgId=$(aws ec2 create-security-group \
            --description "$DB_SG_DESCRIPTION"  \
            --group-name $DB_SG_NAME \
            --vpc-id $vpcId  \
            --tag-specifications "ResourceType=security-group,Tags=[{Key=Name, Value=$DB_SG_NAME}]" \
            | grep GroupId | cut -d '"' -f4)
fi

echo "[*] DB security group: $sgId"


# Adding ingress to DB security group
echo "[*] Adding 3306/tcp from $DB_SG_CIDR_INGRESS ingress to DB security group"
aws ec2 authorize-security-group-ingress \
  --group-id $sgId \
  --protocol tcp \
  --port 3306 \
  --cidr $DB_SG_CIDR_INGRESS 2>/dev/null



# DB subnet group
dbSubnetGroupName=$(aws rds describe-db-subnet-groups \
                      --db-subnet-group-name $DB_SUBNET_GROUP_NAME \
                      --query 'DBSubnetGroups[*].DBSubnetGroupName' \
                      --output text 2> /dev/null)

if [[ -z $dbSubnetGroupName ]]; then
  echo "[+] Creating DB Subnet Group $DB_SUBNET_GROUP_NAME"
  aws rds create-db-subnet-group \
    --db-subnet-group-name $DB_SUBNET_GROUP_NAME \
    --db-subnet-group-description $DB_SUBNET_GROUP_DESCRIPTION \
    --subnet-ids $subnetsId \
    --no-cli-pager --no-paginate
fi


# getting credentials for DB cluster

echo "[*] Creating credentials for Database"
echo -n "User: "
read user
echo -n "Password: "
read password # add -s flag to hide password
echo -e -n "\nConfirm your password: "
read confirmPassword

if [[ $password != $confirmPassword ]];then
  echo "Confirmation password is different!"
  exit 1
fi


# Creating DB cluster (using aurora-mysql engine v5.7)
echo -e "\n[*] Creating $DB_CLUSTER_NAME multi-AZ db cluster"

aws rds create-db-cluster \
  --engine $DB_ENGINE \
  --engine-version $DB_ENGINE_VERSION \
  --db-cluster-identifier $DB_CLUSTER_NAME \
  --db-subnet-group-name $DB_SUBNET_GROUP_NAME \
  --vpc-security-group-ids $sgId \
  --master-username $user \
  --master-user-password $password \
  --storage-encrypted \
  --enable-cloudwatch-logs-exports error \
  --deletion-protection \
  --no-paginate --no-cli-pager \
  | grep -E "DBClusterIdentifier|DBSubnetGroup|EngineVersion|Port"
  

# Creating instances for DB cluster
for i in $(seq $DB_INSTANCES);do
  instanceName="$DB_CLUSTER_NAME-instance-$i"
  
  echo "[+] Creating instance $instanceName for DB cluster"
  aws rds create-db-instance \
    --db-instance-identifier $instanceName  \
    --db-cluster-identifier $DB_CLUSTER_NAME \
    --engine $DB_ENGINE \
    --db-instance-class $DB_CLUSTER_INSTANCES_TYPE \
    --no-paginate --no-cli-pager \
    | grep -E "DBInstanceIdentifier|DBInstanceClass|EngineVersion|PubliclyAccessible"
done

# PROBLEM WITH OPTION (correct value for instance-type?)
# --db-cluster-instance-class $DB_CLUSTER_INSTANCES_TYPE \

# INVALID OPTIONS FOR aurora-mysql
#--monitoring-interval 60 \
# --publicly-accessible <> PubliclyAccessible isn't supported for DB en gine aurora-mysql
# --allocated-storage 10 \

#echo "Waiting for $DB_CLUSTER_NAME DB cluster creation ..."
#aws rds describe-db-clusters \
#  --db-cluster-identifier $dbClusterIdentifier \
#  --filter 'Name=endpoint_type,Values=DescribeDBClusterEndpoints,DescribeDBInstances'
