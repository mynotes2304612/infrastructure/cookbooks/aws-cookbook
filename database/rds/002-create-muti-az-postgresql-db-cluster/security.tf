resource "aws_security_group" "allow_db_access" {
  name        = "DB Security Group"
  description = "Permit access from RDS Security Group"
  vpc_id      = aws_vpc.main.id

  ingress {
    description      = "Postgresql from VPC"
    from_port        = 5432
    to_port          = 5432
    protocol         = "tcp"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "postgres RDS Security Group"
  }
}