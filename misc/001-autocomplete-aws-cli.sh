#!/bin/bash
#
# This script enable autocomplete of aws cli command
#
# EXAMPLE:
# bash misc/001-autocomplete-aws-cli.sh

declare -A shellProfile

# register your prefered shell program (e.g zsh, ...)
shellProfile['bash']="$HOME/.bashrc"

# locating aws_complete executable
echo "[*] Searching for aws_completer"
aws_complete_path=$(whereis -b aws_completer | awk '{ print $2 }')

if [[ ! -e $aws_complete_path ]];then
  aws_complete_path=$(find / -name aws_completer -type f -executable  2> /dev/null | head -n1)

  if [[ ! -e $aws_complete_path ]];then
    echo "[-] aws_completer didn't found"
    exit 1
  fi
fi

TF=$(mktemp)
cat > $TF << EOF
complete -C $aws_complete_path aws
EOF

echo "[*] Enabling temporary autocompletion"
source $TF

shell_name=$(echo $SHELL | rev | cut -d/ -f1 | rev)
read -r -p "Do you want to modify your ${shellProfile[$shell_name]}?[y/n] " answer

case $answer in
    [Yy]* ) 
        echo -e "\n###### AWS AUTOCOMPLETE" | tee -a ${shellProfile[$shell_name]};
        echo "export PATH=\$PATH:$(dirname $aws_complete_path)" | tee -a ${shellProfile[$shell_name]};
        echo "complete -C $aws_complete_path aws" | tee -a  ${shellProfile[$shell_name]};
        ;;
    [Nn]* ) exit;;
esac

echo -e "\nTo enable aws autocomplete run:\n\t source ${shellProfile[$shell_name]}"