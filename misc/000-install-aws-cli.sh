#!/bin/bash
#
# This script install aws cli V2 for Linux x86.64

AWS_CLI_URL="https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip"
BASE_DIR=/tmp

#sudo yum install -y unzip
curl $AWS_CLI_URL -o "$BASE_DIR/awscliv2.zip"
unzip -d $BASE_DIR $BASE_DIR/awscliv2.zip

sudo $BASE_DIR/aws/install