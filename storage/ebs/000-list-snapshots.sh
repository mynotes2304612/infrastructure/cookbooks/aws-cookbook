#!/bin/bash
#
# This script list the taken snapshots of volume attached to a selected EC2 instance


###### SCRIPT VARIBLES ######

INSTANCE_NAME='myInstance'

##############################


volumeId=$(aws ec2 describe-instances \
              --filter "Name=tag:Name,Values=$INSTANCE_NAME" \
              --query 'Reservations[].Instances[].BlockDeviceMappings[].Ebs.VolumeId' \
              --output text)

echo "[*] Listing snapshot of volume $volumeId"

aws ec2 describe-snapshots \
  --filters "Name=volume-id,Values=$volumenId" \
  --query "Snapshots[*].{SnapshotId: SnapshotId, StartTime: StartTime}"
