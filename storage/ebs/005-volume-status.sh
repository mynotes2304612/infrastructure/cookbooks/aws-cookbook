#!/bin/bash
#
# This script monitor the status of attached volumes to an EC2 instance
#
# EXAMPLES:
# - Simple check
#   bash ebs/005-volume-status.sh
#
# - Continuo check
#   watch bash ebs/005-volume-status.sh

###### SCRIPT VARIABLES ######

INSTANCE_NAME="Bastion Host"

##############################

volumesId=$(aws ec2 describe-instances \
              --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
                        "Name=instance-state-name,Values=running" \
              --query 'Reservations[].Instances[].BlockDeviceMappings[].Ebs.VolumeId' \
              --output text)

echo "Attached volumes: $volumesId"

echo "Volumen status - $(date)"
aws ec2 describe-volume-status \
  --volume-ids $volumesId \
  --query "VolumeStatuses[].{VolumeId: VolumeId, 
                              Status: VolumeStatus.Status, 
                              Details: VolumeStatus.Details}" \
  --no-cli-pager --no-paginate