#!/bin/bash
#
# This script create a new volumen on a specified an availability zone


###### SCRIPT VARIBLES ######

VOLUME_NAME='My-Volumen'
VOLUME_TYPE=gp2
VOLUME_SIZE=10

# a volume has to be in the same AZ as the EC2 instance where it is to be mounted
AVAILABILITY_ZONE=us-east-1a

##############################


echo "[+] Creating a new volume (Name: $VOLUME_NAME)"

volumeId=$(aws ec2 describe-volumes \
                --filters "Name=availability-zone,Values=$AVAILABILITY_ZONE"\
                          "Name=tag:Name,Values=$VOLUME_NAME"\
                --query 'Volumes[].VolumeId' \
                --output text)

if [[ -z $volumeId ]];then
  echo "[+] Creating $VOLUME_NAME volume"
  volumeId=$(aws ec2 create-volume \
              --availability-zone $AVAILABILITY_ZONE \
              --size $VOLUME_SIZE \
              --encrypted \
              --volume-type $VOLUME_TYPE \
              --tag-specifications "ResourceType=volume,Tags=[{Key=Name,Value=$VOLUME_NAME}]" \
              --query '.VolumeId' \
              --output text)
fi


echo "[*] Describing created volume (VolumeId: $volumeId)"
aws ec2 describe-volumes \
  --volume-ids $volumeId \
  | grep -E "CreateTime|Size|State|VolumeType|Encrypted"