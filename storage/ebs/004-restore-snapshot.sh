#!/bin/bash
#
# This script restore a taken snapshot and attach restored 
# volume on a selected block devide in a EC2 instance 
#
# Requirements:
# - jq (JSON processor)

###### SCRIPT VARIABLES ######

if [[ $# -lt 1 ]];then
  echo "USAGE: $0 BLOCK_DEVICE_NAME"
  echo -e "EXAMPLE:\n\tbash $0 /dev/xvdc"
  exit 1
fi

BLOCK_DEVICE_NAME=$1
INSTANCE_NAME=Lab
SNAPSHOT_NAME='My-Snapshot'

RESTORED_VOLUME_NAME='Restored-Volume'

##############################

# getting ID of Lab instance
describeInstance=$(aws ec2 describe-instances \
                    --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
                              "Name=instance-state-name,Values=running" \
                    --query 'Reservations[].Instances[]' \
                    --output json | jq '.[]')
az=$(echo $describeInstance | jq -r '.Placement.AvailabilityZone')
instanceId=$(echo $describeInstance | jq -r '.InstanceId')

# getting ID of snapshot
snapshotId=$(aws ec2 describe-snapshots \
              --filters "Name=tag:Name,Values=$SNAPSHOT_NAME" \
              --query 'Snapshots[].SnapshotId' \
              --output text)

# creating a volumen using created snapshot
echo "[*]  Creating $RESTORED_VOLUME_NAME volume from $SNAPSHOT_NAME snapshot"
volumeId=$(aws ec2 create-volume \
            --snapshot-id $snapshotId \
            --volume-type gp2 \
            --availability-zone $az \
            --tag-specifications "ResourceType=volume,Tags=[{Key=Name,Value=$RESTORED_VOLUME_NAME}]" \
            --query 'VolumeId' \
            --output text)

echo "[*] Waiting until volume $volumeId is available"

volumeState='unknown'
while [[ $volumeState != available ]]
do
  volumeState=$(aws ec2 describe-volumes \
                  --volume-ids $volumeId \
                  --query 'Volumes[].State' \
                  --output text)
  echo "Volume $volumeId state: $volumeState"
  sleep 1
done

echo "[*] Attaching $RESTORED_VOLUME_NAME volume 
          to $INSTANCE_NAME on $BLOCK_DEVICE_NAME 
          (VolumeId: $volumeId, instanceId: $instanceId)"

aws ec2 attach-volume \
  --device $BLOCK_DEVICE_NAME \
  --volume-id $volumeId \
  --instance-id  $instanceId