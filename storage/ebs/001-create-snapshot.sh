#!/bin/bash
#
# This script take a snapshot of the attached volume of a selected EC2 instance
#
# NOTE:
# - This script stop your instance before task the snapshot


###### SCRIPT VARIBLES ######

INSTANCE_NAME='myInstance'

##############################


# Getting ID of EBS disk attached to Processor instance
volumeId=$(aws ec2 describe-instances \
              --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
                        "Name=instance-state-name,Values=running" \
              --query 'Reservations[].Instances[].BlockDeviceMappings[].Ebs.VolumeId' \
              --output text)

instanceId=$(aws ec2 describe-instances \
              --filters 'Name=tag:Name,Values=$INSTANCE_NAME' \
                        "Name=instance-state-name,Values=running" \
              --query 'Reservations[0].Instances[0].InstanceId' \
              --output text)


echo "[*] Stopping instance $instanceId"
aws ec2 stop-instances --instance-ids $instanceId


# waiting until instance is completely stopped
echo "[*] Waiting until instance $instanceId is completely stopped ..."
aws ec2 wait instance-stopped --instance-id $instanceId


echo "[+] Creating snapshot of disk $volumeId"
snapshotId=$(aws ec2 create-snapshot \
              --volume-id $volumeId \
              --query 'SnapshotId' \
              --output text)


echo "[*] Waiting until snapshot $snapshotId fo volume $volumeId is complete ..."
aws ec2 wait snapshot-completed --snapshot-id $snapshotId


echo "[*] Starting instance $instanceId"
aws ec2 start-instances --instance-ids $instanceId


echo "[*] Waiting for instance $instanceId ..."
aws ec2 wait instance-running --instance-id $instanceId
