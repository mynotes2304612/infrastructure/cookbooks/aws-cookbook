#!/bin/bash
#
# This script create an EFS file system

###### SCRIPT VARIABLES ######

FILE_SYSTEM_NAME=myEFS
PERFORMANCE_MODE=generalPurpose
AVAILABILITY_ZONE=us-east-1a

##############################

aws efs create-file-system \
  --performance-mode $PERFORMANCE_MODE \
  --encrypted \
  --tags "{\"Key\": \"Name\", \"Value\": \"$FILE_SYSTEM_NAME\"}"