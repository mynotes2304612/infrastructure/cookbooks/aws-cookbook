#!/bin/bash
#
# This script create a static website on a S3 bucket

if [[ $# -le 1 ]];then
  echo "USAGE: $0 STATIC_WEBSITE_LOCATION"
  exit 1
fi

STATIC_WEBSITE_LOCATION=$1
BUCKET_NAME="static-website-$RANDOM"
LOCATION=us-east-1

source 000-create-bucket.sh $BUCKET_NAME $LOCATION

echo "[+] Setting a website configuration for $BUCKET_NAME bucket"
aws s3 website s3://$BUCKET_NAME/ --index-document index.html

echo "[+] Loading files from $STATIC_WEBSITE_LOCATION to $BUCKET_NAME bucket"
aws s3 sync $STATIC_WEBSITE_LOCATION s3://$BUCKET_NAME/ --acl public-read

echo "[*] Listing website files on bucket"
aws s3 ls s3://$BUCKET_NAME