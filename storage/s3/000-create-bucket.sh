#!/bin/bash
#
# This script create an S3 bucket on an specified region


if [[ $# -le 1 ]];then
  echo """
      USAGE: $0 BUCKET_NAME [LOCATION]

      EXAMPLES:
        bash $0 example9001
  """
  exit 1
fi
BUCKET_NAME=$1

LOCATION=us-east-1
if [[ -z $2 ]];then
  LOCATION=$2
fi


aws s3api create-bucket \
  --bucket $BUCKET_NAME \
  --region $LOCATION \
  --create-bucket-configuration LocationConstraint=$LOCATION 2> /dev/null