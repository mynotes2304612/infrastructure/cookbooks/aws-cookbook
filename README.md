## AWS Recipes

Here you can find some reusable scripts to deploy AWS infrastructure using `AWS CLI`, `AWS SDK` or `terraform`.

> These scripts use general notation, so you can include them in your our scripts to deploy your infrastructure.

How does recipes are structured?
```text
|-compute
|   |- ec2
|   |   |- 001-create-ami.sh
|   |   |- 002-describe-instance.sh
|   |   |- ...  
|   |- lambda
|
|- database
|   |- rds
|...
```



`Number of recipies`: 25 
> Count recipies  with `find . -name "[0-9]*-*" -maxdepth 3 2> /dev/null | wc -l`
