#!/bin/bash
# Student: Lozano Gustavo
#
# This script automate the installation of Amazon Cloud Watch Agent
# on EC2 instance

WAIT_FOR_COMMAND_TIME=25 # sleep time until we query again the send command status
INSTANCE_NAME_TAG="Web Server"

instanceId=$(aws ec2 describe-instances \
              --filter "Name=tag:Name,Values=$INSTANCE_NAME_TAG" \
              --query 'Reservations[].Instances[].InstanceId' \
              --output text)
echo "[*] $INSTANCE_NAME_TAG ID: $instanceId"

# Installing AWS Cloud Watch Agent on Web Server EC2 instance
# NOTE: To know what parameter a document is expecting check
#       them with aws ssm describe-document --name DOCUMENT_NAME

DOCUMENT_NAME="AWS-ConfigureAWSPackage"
echo "[*] Sending $DOCUMENT_NAME document to $instanceId instance"
sendCommandOutput=$(aws ssm send-command --instance-ids $instanceId \
                    --document-name $DOCUMENT_NAME \
                    --parameters "action=Install,name=AmazonCloudWatchAgent,version=latest" --output json)

mkdir -p tmp
echo $sendCommandOutput > tmp/$DOCUMENT_NAME.log # only for debug purpose

echo "[*] Output after send $DOCUMENT_NAME command to $instanceId instance"
echo $sendCommandOutput | jq '.Command | { Instances: .InstanceIds, Targets: .Targets}'
echo $sendCommandOutput | jq | grep -E "Status|MaxErrors|DocumentName"

echo "Sleeping $WAIT_FOR_COMMAND_TIME ..."
sleep $WAIT_FOR_COMMAND_TIME
commandId=$(echo $sendCommandOutput | jq -r '.Command.CommandId')

echo "Quering status of command invocation $commandId"
aws ssm list-command-invocations \
    --command-id $commandId \
    --instance-id $instanceId \
    | grep "Status|StatusDetails|StandardOutputContent|ResponseCode" 2> /dev/null

# Create Monitor-Web-Server parameter
PARAMETER_NAME="Monitor-Web-Server"
PARAMETER_DESCRIPTION="Collect web logs and systems metrics"

echo "[*] Generating configuration for $PARAMETER_NAME"
sed "s/@1/$instanceId/g" Monitor-Web-Server.example > Monitor-Web-Server.json
aws ssm put-parameter \
    --name $PARAMETER_NAME \
    --type String \
    --value file://Monitor-Web-Server.json \
    --overwrite

# Staring AWS Cloud Watch Agent
DOCUMENT_NAME="AmazonCloudWatch-ManageAgent"
# DEFAULT PARAMETERS FOR AmazonCloudWatch-ManageAgent
#--parameters "action=configure,
#                                  mode=ec2,
#                                  optionalConfigurationSource=ssm,
#                                  optionalConfigurationLocation='Monitor-Web-Server',
#                                  optinalRestart=yes" \

echo "[*] Sending $DOCUMENT_NAME document to $instanceId instance"
sendCommandOutput=$(aws ssm send-command \
                    --instance-ids $instanceId \
                    --document-name $DOCUMENT_NAME \
                    --parameters "optionalConfigurationLocation=Monitor-Web-Server" \
                    --output json)

echo $sendCommandOutput > tmp/$DOCUMENT_NAME.log # only for debug purpose

echo "[*] Output after send $DOCUMENT_NAME command to $instanceId instance"
echo $sendCommandOutput | jq '.Command | { Instances: .InstanceIds, Targets: .Targets}'
echo $sendCommandOutput | jq | grep -E "Status|MaxErrors|DocumentName"

echo "Sleeping $WAIT_FOR_COMMAND_TIME ..."
sleep $WAIT_FOR_COMMAND_TIME
commandId=$(echo $sendCommandOutput | jq -r '.Command.CommandId')

echo "Quering status of command invocation $commandId"
aws ssm list-command-invocations \
    --command-id $commandId \
    --instance-id $instanceId \
    | grep "Status|StatusDetails|StandardOutputContent|ResponseCode" 2> /dev/null
