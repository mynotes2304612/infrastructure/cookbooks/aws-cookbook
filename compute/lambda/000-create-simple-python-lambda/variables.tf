variable "example_layer_zip" {
  type = string
  default = "tmp/example-layer.zip"
}

variable "lambda_runtime" {
  type = string
  default = "python3.8"
}