data "archive_file" "source_code" {
  type          = "zip"
  source_dir   = "./mycode/"
  output_path   = "./tmp/mycode.zip"
}

data "aws_iam_role" "role_for_lamba" {
  name = "LabRole"
}

resource "aws_lambda_layer_version" "lambda_layer" {
  filename   = var.example_layer_zip
  layer_name = "example-layer"

  compatible_runtimes = [var.lambda_runtime]
  source_code_hash = filebase64sha256(var.example_layer_zip)
}

resource "aws_lambda_function" "lambda_function" {
  filename      = data.archive_file.source_code.output_path
  function_name = "example_lambda" # name of lambda function
  role          = data.aws_iam_role.role_for_lamba.arn # attach a role to lambda function
  handler       = "main.lambda_handler" # python function to be called to handle events
  layers        = [aws_lambda_layer_version.lambda_layer.arn]
  
  runtime = var.lambda_runtime
  source_code_hash = data.archive_file.source_code.output_base64sha256
}

# This URL is just for testing purpose
resource "aws_lambda_function_url" "lambda_function_url" {
  function_name      = aws_lambda_function.lambda_function.function_name
  authorization_type = "NONE"
}

output "aws_lambda_function_url" {
  value = aws_lambda_function_url.lambda_function_url.function_url
}
