#!/bin/sh
#
# This script install the requirements in a virtual enviroment (on a container matching runtime version) and 
# then wrap installed packages in a zip file (required by terraform)

if [[ $# -lt 1 ]];then
  echo "USAGE: $0 LAMBDA_LAYER_NAME"
  exit 1
fi

LAMBDA_LAYER_NAME=$1
ENV_NAME=python

echo "[*] Creating a virtual enviroment"
python3 -m venv --prompt lambda $ENV_NAME

REQUIREMENTS_FILE=requirements.txt
echo "[+] Installing dependencies of $REQUIREMENTS_FILE file"
$ENV_NAME/bin/pip install -r $REQUIREMENTS_FILE