#!/usr/bin/env python3
#
# This script get information of a client based on its IP
#
# Final Purpose:
# This script was written to make our code dependend on some libraries (requests and pytimeparse) 
#   to give us motivation to create lambda layer

import json
import requests
from datetime import datetime, timedelta
from pytimeparse.timeparse import timeparse


def get_client_information(ip) -> dict:
    """
    Get information of a client based on its IP

    Returns: {'timezone': CLIENT_TIMEZONE, 'localtime': CLIENT_LOCAL_TIME}
    """
    r = requests.get(f"http://worldtimeapi.org/api/ip/{ip}")
    response = r.json()

    utc_datetime = datetime.fromisoformat(response['utc_datetime'])
    utc_offset = timeparse(response['utc_offset'], granularity='minutes')

    if utc_offset < 0:
        local_datetime = utc_datetime - timedelta(minutes=-utc_offset)
    else:
        local_datetime = utc_datetime + timedelta(minutes=utc_offset)

    return {'timezone': response['timezone'], 'localtime': local_datetime.isoformat()}
