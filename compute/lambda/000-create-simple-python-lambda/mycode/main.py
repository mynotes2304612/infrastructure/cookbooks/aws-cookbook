#!/usr/bin/env python3

import boto3

from information import get_client_information # custom function

ec2 = boto3.client('ec2')

def lambda_handler(event, context): # function to handler requests
    client_ip = event['requestContext']['http']['sourceIp']
    client_information = get_client_information(client_ip)

    client_information['ip'] = client_ip

    return str(client_information)
