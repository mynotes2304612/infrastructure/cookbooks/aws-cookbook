#!/bin/bash
#
# This script install the requirements in a virtual enviroment (on a container matching runtime version) and 
# then wrap the installed packages in a zip file (required by terraform)

###### SCRIPT VARIBLES ######

# to select RUNTIME, refer to https://hub.docker.com/r/amazon/aws-lambda-python
RUNTIME='python:3.8'
CONTAINER_NAME=lambda_runtime
TMP_DIR=tmp

LAMBDA_LAYER_NAME='example-layer' # a file called $LAMBDA_LAYER_NAME.zip must be create in $TMP_DIR

INSTALL_REQUIREMENTS_SCRIPT=01-install-requirements.sh
# modify here you have problem executing 01-install-requirements.sh script in your enviroment
EXEC_SCRIPT="bash $INSTALL_REQUIREMENTS_SCRIPT $LAMBDA_LAYER_NAME"

##############################


mkdir -p $TMP_DIR # create tmp directoy to mount into container

echo "[*] Creating an enviroment using $RUNTIME image"
LOOP_CMD="tail -f /dev/null" # this command is just to keep the container alive
docker run -d -v $PWD/$TMP_DIR:/tmp --workdir /tmp --name $CONTAINER_NAME  $RUNTIME $LOOP_CMD

echo "[*] Copying file into the container"
docker cp requirements.txt $CONTAINER_NAME:/tmp/
docker cp $INSTALL_REQUIREMENTS_SCRIPT $CONTAINER_NAME:/tmp/

echo "[*] Executing $SCRIPT_NAME script on container"
docker exec $CONTAINER_NAME $EXEC_SCRIPT

echo "[*] Wrapping dependencies"
python_version=$(docker exec $CONTAINER_NAME python3 -V | grep -E "[1-9]\.[0-9]*" -o)
# Check lambda layer directory hierarchy: 
# - https://docs.aws.amazon.com/lambda/latest/dg/configuration-layers.html
cd $TMP_DIR && zip -r $LAMBDA_LAYER_NAME.zip python/lib/python$python_version/site-packages

echo "[+] Layer file was created: $TMP_DIR/$LAMBDA_LAYER_NAME.zip"

echo "[*] Destroying container $CONTAINER_NAME"
docker kill $CONTAINER_NAME  2> /dev/null
docker rm $CONTAINER_NAME  2> /dev/null