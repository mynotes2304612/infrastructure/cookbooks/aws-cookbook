## Simple Lambda Function

This recipe show you how to deploy a simple lambda function that rely on a lambda layer using terraform 

The source code of this function is in `mycode` directory.

The function `lambda_handler` of `mycode/main.py` script is called to process each request that trigger the lambda function.

### Usage

```sh
# deploy a container that match lambda function runtime and 
# install the requirements in a virtual enviroment
# then wrap installed packages in a zip file (required by terraform)
bash 00-create-lambda-layer.sh

# deploy lambda function
terraform init 
terraform apply
```

or 

```sh
bash deploy.sh
```