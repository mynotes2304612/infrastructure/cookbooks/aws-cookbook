#!/bin/bash
#
# This script launch an EC2 instance on selected VPC, attached to it an SSH security group (created).
# This instance is generaly used to work as pivot to reach other private instance.


################################ SCRIPT VARIBLES ################################

INSTANCE_NAME="mypivot-012"
VPC_ID="vpc-0363106c0c97fa498" # to avoid duplications (VPC with the same Name tag)
SUBNET_ID="subnet-01c58e2b74ede1a6b"
ATTACH_ELASTIC_IP=true # don't forget to release the attached elastic IP
INSTANCE_TYPE='t2.small'
KEY_PAIR='college-nvirg'

################################################################################################


# Selecting the AMI using SSM

ami=$(aws ssm get-parameter \
        --name /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2 \
        --query "Parameter.Value" \
        --output text)

echo "AMI $ami:"

# describing the selected AMI
aws ec2 describe-images --image-ids $ami \
  | grep -E "Architecture|Platform|Description|Name"

# Getting ID of VPC
#vpcId=$(aws ec2 describe-vpcs \
#					--filters "Name=tag:Name,Values=$VPC_NAME" \
#					--query 'Vpcs[*].VpcId' \
#					--output text)
vpcId=$VPC_ID

echo "VPC: $vpcId"

subnetId=$SUBNET_ID

echo "subnetId: $subnetId"


# creating an SSH security group
sg_name="allow-ssh-$RANDOM"
echo "[*] Creating $sg_name security group"
sgId=$(aws ec2 create-security-group \
          --group-name $sg_name \
          --description "Allow SSH Access" \
          --query "GroupId" \
          --vpc-id $vpcId \
          --output text)

echo "Security group $sg_name: $sgId"

# Adding ingress to SSH security group
aws ec2 authorize-security-group-ingress \
  --group-id $sgId \
  --protocol tcp \
  --port 22 \
  --cidr '0.0.0.0/0' \
  --no-paginate --no-cli-pager 2> /dev/null

echo "[*] Security group $sg_name ingress"
aws ec2 describe-security-groups \
  --filters "Name=vpc-id,Values=$vpcId" \
            "Name=group-name,Values=$sg_name" \
  --query "SecurityGroups[].IpPermissions" \
  --no-paginate --no-cli-pager 


# Lunching instance
instanceId=$(aws ec2 describe-instances \
              --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
                        "Name=instance-state-name,Values=running" \
              --query 'Reservations[*].Instances[*].InstanceId' \
              --output text)

if [[ -z $instanceId ]]; then
  echo "Creating instance $INSTANCE_NAME"
  instanceId=$(aws ec2 run-instances \
                --image-id $ami \
                --instance-type $INSTANCE_TYPE \
                --key-name $KEY_PAIR \
                --security-group-ids $sgId \
                --subnet-id $subnetId \
                --associate-public-ip-address \
                --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=$INSTANCE_NAME}]" \
                --query 'Instances[*].InstanceId' \
                --output text)
fi

echo "$INSTANCE_NAME instance ($instanceId)"
echo "Waiting for instance ..."
aws ec2 wait instance-running --instance-ids $instanceId

if $ATTACH_ELASTIC_IP ;then
  allocationId=$(aws ec2 allocate-address \
                  --query 'AllocationId' \
                  --output text)

  aws ec2 associate-address \
    --allocation-id $allocationId \
    --instance-id $instanceId
fi

aws ec2 describe-instances --instance-ids $instanceId \
    --filters 'Name=instance-state-name,Values=running' \
    --query 'Reservations[*].Instances[*]' \
    | grep -E "KeyName|PublicIpAddress|Platform|PublicDnsName" | uniq
