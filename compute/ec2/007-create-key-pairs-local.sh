#!/bin/bash
#
# This script create a new key-pair in a selected region
# A diference of ec2/006-create-key-pairs.sh recipe, in this recipe we create the 
# the key-pair localy, so you can have more control at the creation momment.
#
# EXAMPLES:
# - bash ec2/007-create-key-pairs-local.sh example us-east-1

###### SCRIPT VARIABLES ######

if [[ $# -lt 2 ]];then
  echo "USAGE: $0 KEY_NAME REGION [KEY_BITES]"
  exit 1
fi

KEY_NAME=$1
REGION=$2
KEY_BITES=$3

if [[ -z $KEY_BITES ]];then
  KEY_BITES=4096
fi

##############################

KEY_TYPE=rsa
echo "[+] Creating a $KEY_TYPE key with $KEY_BITES bites"
ssh-keygen -m PEM -q -b $KEY_BITES -t $KEY_TYPE -f $KEY_NAME.pem
echo "[*] Secret key: $KEY_NAME.pem - Public key: $KEY_NAME.pem.pub"

echo "[*] Importing public key of $KEY_NAME key-pair to AWS"
aws ec2 import-key-pair \
  --key-name $KEY_NAME \
  --region $REGION \
  --public-key-material fileb://$PWD/$KEY_NAME.pem.pub