#!/bin/bash
#
# Revision: May 16, 2023
#
# Script to launch an EC2 instance of a selected type on a selected subnet of a VPC.
# Attaching to it selected security group and key-pair.



################################ SCRIPT VARIBLES ################################

### network
VPC_NAME="Work VPC"
SUBNET_NAME='Work Public Subnet'

### security group
SECURITY_GROUP_NAME="Ec2SecurityGroup"

### instance
INSTANCE_NAME="myInstance"
INSTANCE_TYPE='t2.micro'
KEY_PAIR='vockey'
################################################################################################

# Selecting the AMI using SSM
ami=$(aws ssm get-parameter \
        --name /aws/service/ami-amazon-linux-latest/amzn2-ami-hvm-x86_64-gp2 \
        --query "Parameter.Value" \
        --output text)

echo "AMI $ami:"

# describing the selected AMI
aws ec2 describe-images --image-ids $ami \
  | grep -E "Architecture|Platform|Description|Name"

# VPC
vpcId=$(aws ec2 describe-vpcs \
        --filters "Name=tag:Name,Values=$VPC_NAME" \
        --query 'Vpcs[*].VpcId' \
        --output text)

echo "VPC: $vpcId"

# SubNetwork - selecting first subnet of VPC (for simplicity)
subnetId=$(aws ec2 describe-subnets \
            --filters "Name=vpc-id,Values=$vpcId" \
                      "Name=tag:Name,Values=$SUBNET_NAME" \
            --query 'Subnets[].SubnetId' \
            --output text)

echo "Subnet $subnetId :"
aws ec2 describe-subnets --subnet-ids $subnetId \
  | grep -E "AvailabilityZone|CidrBlock|VpcId"


## Security group
sgId=$(aws ec2 describe-security-groups \
          --query "SecurityGroups[?GroupName==\`$SECURITY_GROUP_NAME\`].GroupId" \
          --output text)


echo "Security group $SECURITY_GROUP_NAME id: $sgId"
aws ec2 describe-security-groups \
    --group-ids $sgId \
    --query "SecurityGroups[*].IpPermissions" \
    --no-cli-pager --no-paginate


# Lunching instance
instanceId=$(aws ec2 describe-instances \
              --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
                        "Name=instance-state-name,Values=running" \
              --query 'Reservations[*].Instances[*].InstanceId' \
              --output text)

if [[ -z $instanceId ]]; then
  echo "Creating instance $INSTANCE_NAME"
  instanceId=$(aws ec2 run-instances \
                --image-id $ami \
                --instance-type $INSTANCE_TYPE \
                --key-name $KEY_PAIR \
                --security-group-ids $sgId \
                --subnet-id $subnetId \
                --associate-public-ip-address \
                --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=$INSTANCE_NAME}]" \
                --query 'Instances[*].InstanceId' \
                --output text)
                
  # Applicable options: 
  #                 --user-data file://$PWD/UserData.txt \
fi

echo "$INSTANCE_NAME instance ($instanceId)"
echo "Waiting for instance ..."
aws ec2 wait instance-running --instance-ids $instanceId

aws ec2 describe-instances --instance-ids $instanceId \
    --filters 'Name=instance-state-name,Values=running' \
    --query 'Reservations[*].Instances[*]' \
    | grep -E "KeyName|PublicIpAddress|Platform|PublicDnsName" | uniq