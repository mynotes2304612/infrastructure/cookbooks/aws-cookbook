#!/bin/bash
#
# This script create a new key-pair in a selected region

###### SCRIPT VARIABLES ######

if [[ $# -lt 2 ]];then
  echo "USAGE: $0 KEY_NAME REGION"
  exit 1
fi

KEY_NAME=$1
REGION=$2

KEY_FORMAT=pem
KEY_TYPE=rsa

##############################


# Verifing if homework1 key exist
key_pair=$(aws ec2 describe-key-pairs \
            --key-names $KEY_NAME \
            --region $REGION \
            --query 'KeyPairs[].KeyName' \
            --output text)

if [[ -z $key_pair ]]; then
  echo "Creating $KEY_NAME $KEY_TYPE key pair on region $REGION"
  aws ec2 create-key-pair \
    --key-name $KEY_NAME \
    --query 'KeyMaterial' \
    --key-format $KEY_FORMAT \
    --key-type $KEY_TYPE \
    --output text > $KEY_NAME.$KEY_FORMAT

  echo "Key-Pair Location: $PWD/$KEY_NAME.$KEY_FORMAT"
fi
