#!/bin/bash
#
# This script describe a running EC2 instance
#
# Requirements:
# - AWS CLI v2
# - jq (JSON parser)

################################ SCRIPT VARIABLES ################################

if [[ $# -lt 1 ]];then
  echo "USAGE: $0 INSTANCE_NAME"
  exit 1
fi

INSTANCE_NAME=$1 # instance to describe

################################################################################################


describeInstance=$(aws ec2 describe-instances \
                    --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
                              "Name=instance-state-name,Values=running" \
                    --query 'Reservations[].Instances[]' | jq '.[]')

# AvailabilityZone
az=$(echo $describeInstance  \
      | jq '.Placement.AvailabilityZone')

echo "$INSTANCE_NAME AvailabilityZone: $az"

# AMI
instanceAMI=$(echo $describeInstance  \
      | jq -r '.ImageId')

echo "$INSTANCE_NAME AMI: $instanceAMI"
echo "AMI $instanceAMI description:"
aws ec2 describe-images \
  --image-ids $instanceAMI \
  --query 'Images[].Description'

# volume
volumeId=$(echo $describeInstance \
            | jq -r '.BlockDeviceMappings[].Ebs.VolumeId')

echo "$INSTANCE_NAME volume: $volumeId"
echo "Volume $volumeId description:"
aws ec2 describe-volumes \
  --volume-ids $volumeId \
  | grep -E "Size|VolumeType|Avai|Device"

# security group
sgId=$(echo $describeInstance \
        | jq -r '.SecurityGroups[].GroupId')

echo "$INSTANCE_NAME security group: $sgId"
echo "Security group $sgId ingress:"
aws ec2 describe-security-groups \
  --group-ids $sgId \
  --query 'SecurityGroups[].IpPermissions' \
  --no-paginate \
  --no-cli-pager

# connectivity
echo "Connectivity information of $INSTANCE_NAME instance"
echo $describeInstance | jq | grep -E 'PublicIpAddress|KeyName'
