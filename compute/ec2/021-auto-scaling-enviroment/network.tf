## VPC
data "aws_vpc" "lab_vpc" {
  filter {
    name = "tag:Name"
    values = ["Lab VPC"]
  }
}


## VPC - subnets
data "aws_subnet" "lab_public_subnet1" {
  filter {
    name = "tag:Name"
    values = ["Public Subnet 1"]
  }
}

data "aws_subnet" "lab_public_subnet2" {
  filter {
    name = "tag:Name"
    values = ["Public Subnet 2"]
  }
}

data "aws_subnet" "lab_private_subnet1" {
  filter {
    name = "tag:Name"
    values = ["Private Subnet 1"]
  }
}

data "aws_subnet" "lab_private_subnet2" {
  filter {
    name = "tag:Name"
    values = ["Private Subnet 2"]
  }
}


## Target Group
resource "aws_lb_target_group" "lab_target_group" {
  name        = "LabGroup"
  target_type =  "instance"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = data.aws_vpc.lab_vpc.id
}


## Application Load Balancer
resource "aws_lb" "lab_elb" {
  name               = "LabELB"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [data.aws_security_group.web_security_group.id]
  subnets            = [data.aws_subnet.lab_public_subnet1.id, 
                          data.aws_subnet.lab_public_subnet2.id]

  depends_on         = [aws_lb_target_group.lab_target_group]

  enable_deletion_protection = false # change to true on final deployment
}


## Load Balancer Listener
resource "aws_lb_listener" "lab_lb_listener" {
  load_balancer_arn = aws_lb.lab_elb.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.lab_target_group.arn
  }
}