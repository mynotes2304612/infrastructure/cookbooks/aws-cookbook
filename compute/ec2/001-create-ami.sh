#!/bin/bash
#
# This script was written to automate the following task:
# - Task 1: Create an AMI for Auto Scaling
# of "Lab 6: Scale and Load Balance Your Architecture" (AWS Cloud Foundation)

################################ SCRIPT VARIABLES ################################

INSTANCE_NAME='myInstance'
AMI_NAME=myAMI
AMI_DESCRIPTION="some-description-for-myAMI"

################################################################################################

instanceId=$(aws ec2 describe-instances \
              --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
              --query 'Reservations[].Instances[].InstanceId' \
              --output text)


echo "[*] Creating an AMI from $INSTANCE_NAME instance (InstanceId: $instanceId)"
ami=$(aws ec2 create-image \
      --name $AMI_NAME  \
      --description "$AMI_DESCRIPTION" \
      --instance-id $instanceId \
      --query 'ImageId' \
      --output text)

echo "[*] Inspecting created AMI (ImageId: $ami)"
aws ec2 describe-images \
  --image-ids $ami \
  | grep -E 'Name|Description|PlatformDetails'