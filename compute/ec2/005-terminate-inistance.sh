#!/bin/bash
#
# This script terminate an EC2 instance
#
# Requirements:
# - AWS CLI v2


################################ SCRIPT VARIABLES ################################

INSTANCE_NAME='myInstance' # instance to be terminated

################################################################################################


instanceId=$(aws ec2 describe-instances \
              --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
                        "Name=instance-state-name,Values=running" \
              --query 'Reservations[].Instances[].InstanceId' \
              --output text)

echo "[*] Terminating $INSTANCE_NAME instance (InstanceId: $instanceId)"
aws ec2 terminate-instances \
  --instance-ids $instanceId


