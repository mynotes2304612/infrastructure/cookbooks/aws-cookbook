#!/bin/bash


# # SSH security group
# ssh_sg=$(aws ec2 describe-security-groups \
#           --group-names allowSSH \
#           --query "SecurityGroups[*].GroupId" \
#           --output text 2> /dev/null)

# if [[ -z $ssh_sg ]]; then
#     echo "[*] Creating allowSSH security group"
#     ssh_sg=$(aws ec2 create-security-group \
#                 --group-name allowSSH \
#                 --description "Allow SSH Access" \
#                 --query "GroupId" \
#                 --vpc-id $vpcId \
#                 --output text)
# fi

# echo "SSH security group: $ssh_sg"

# # Adding ingress to SSH security group
# aws ec2 authorize-security-group-ingress \
#   --group-id $ssh_sg \
#   --protocol tcp \
#   --port 22 \
#   --cidr 0.0.0.0/0 2> /dev/null
